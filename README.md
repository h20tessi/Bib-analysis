# Bib-analysis
Simple python script to perform some analysis and visualization of your bibliography.

---

**Script by Hugo TESSIER**

Beforehand, you need to prepare a simple text file containing, on each row, the ArXiv ID of each of the papers contained
in your bibliography.
Then pass the path of this file as an argument to this script and it will generate two main outputs :
    
- A PDF presenting a graph that summarize the citations between the papers of your bibliography and those they
    cite themselves.
    
- A text file that shows the exact number of time each of these papers are cited by the one of your bibliography.

These allow to determine, according to your bibliography, what are its most important papers as well as what may be
the most important papers that are not included in your bibliography yet. As the data are queried with HTTP requests,
the script needs an internet connection to work properly.

On the graph, the blue nodes are the papers that belong to your bibliography. The red ones are the papers, cited by the
blue ones, that are more cited than the most cited blue one : they supposedly are those not to miss. The orange ones
are similar to the red ones, except that they are more cited than the average of the blue ones + the standard deviation
of the citations of the blue ones : they are those which are cited an above-average number of time, hence they are
likely to deserve attention. The grey ones are all the other papers.